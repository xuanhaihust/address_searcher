from abc import ABC, abstractmethod
import pandas as pd
import os

class Database(ABC):

    @abstractmethod
    def get_address_dict(self):
        """
        Address raw data as a dictionary:
        {
            "province": [
                {
                    "name": "Hà Nội",
                    "id": 1
                },
                ...
            ],
            "district": [...],
            "ward": [...],
            "special": [...]
        }
        """
        address_dict = None
        return address_dict


class SQLDatabase(Database):
    def get_address_dict(self):
        pass


class MongoDatabase(Database):
    def get_address_dict(self):
        pass


class ExcelDatabase(Database):
    """
    Address stored in Excel file with known structure.
    """
    def __int__(self, file_path):
        if os.path.splitext(file_path)[1] == ".csv":
            self.df = pd.read_csv(file_path)
        elif os.path.splitext(file_path)[1] == ".xlsx":
            self.df = pd.read_excel(file_path)

    def get_address_dict(self):
        ad_dict = dict()
        ad_dict['province'] =
