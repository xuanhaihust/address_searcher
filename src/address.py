# Implement address node and address tree

from src.tree import GenericTreeNode, GenericTree
from utils.text import TextNormalizer


class Address:
    """
    Address Objects Class.
    """
    def __init__(self, name, address_id, address_type, normalize=True):
        if normalize:
            self.__name = self.normalize(name)
        else:
            self.__name = name
        self.__address_type = address_type
        self.__id = address_id

    @staticmethod
    def normalize(string):
        return TextNormalizer.normalize(string)

    def address_type(self):
        """
        Getter for self.__address_type
        """
        return self.__address_type

    @property
    def name(self):
        return self.__name

    def set_name(self, new_name):
        self.__name = new_name

    @property
    def id(self):
        return self.__id


class AddressFactory:
    @staticmethod
    def create_province(name):
        return Address(name, address_type='province')

    @staticmethod
    def create_district(name):
        return Address(name, address_type='district')

    @staticmethod
    def create_ward(name):
        return Address(name, address_type='ward')

    @staticmethod
    def create_special_address(name):
        return Address(name, address_type='special')


class AddressNode(GenericTreeNode):
    def __int__(self, address: Address,
                children: GenericTreeNode = None,
                parent: GenericTreeNode = None):

        super().__init__(children, parent)


class AddressTree(GenericTree):
    def __init__(self):
        super().__init__()
