import json
import re
from abc import ABC, abstractmethod


class AddressParser:
    """
    Address parser tools class
    """

    @staticmethod
    def origin_acronym_parser(string: str):
        """
        extract origin and acronym string from combined string (results of process() func)
        :param string: address string in "origin (acronym)" pattern
        :return: origin and acronym string
        """
        # extract acronym
        try:
            acronym = re.search(r'\((.+)\)', string).group(1)
        except AttributeError:
            acronym = ''
        # extract origin
        origin = string.replace('(' + acronym + ')', '').strip()

        return origin, acronym

    @classmethod
    def address_parser(cls, string: str) -> list:
        """
        extract origin and acronym string from combined string (results of process() func)
        :param string: special address string in "origin (acronym)" pattern
        :return: origin and acronym string
        """
        # separate official address name and its keywords
        try:
            addresses = re.findall(r'[\w\s\(\)-/,$^]+', string)
        except AttributeError:
            addresses = [string]

        return list(map(cls.origin_acronym_parser, addresses))


class NormUnicode:
    """
    some function for address string pre-processing problem, import to use
    """

    _MAP_DICT_UNICODE = {'à': u'à', 'á': u'á', 'ả': u'ả', 'ã': u'ã', 'ạ': u'ạ', 'ằ': u'ằ', 'ắ': u'ắ', 'ẳ': u'ẳ',
                         'ẵ': u'ẵ', 'ặ': u'ặ', 'ầ': u'ầ',
                         'ấ': u'ấ', 'ẩ': u'ẩ', 'ẫ': u'ẫ', 'ậ': u'ậ', 'è': u'è', 'é': u'é', 'ẻ': u'ẻ', 'ẽ': u'ẽ',
                         'ẹ': u'ẹ', 'ề': u'ề', 'ế': u'ế', 'ể': u'ể', 'ễ': u'ễ',
                         'ệ': u'ệ', 'ò': u'ò', 'ó': u'ó', 'ỏ': u'ỏ', 'õ': u'õ', 'ọ': u'ọ', 'ồ': u'ồ', 'ố': u'ố',
                         'ổ': u'ổ', 'ỗ': u'ỗ', 'ộ': u'ộ', 'ờ': u'ờ', 'ớ': u'ớ',
                         'ở': u'ở', 'ỡ': u'ỡ', 'ợ': u'ợ', 'ù': u'ù', 'ú': u'ú', 'ủ': u'ủ', 'ũ': u'ũ', 'ụ': u'ụ',
                         'ừ': u'ừ', 'ứ': u'ứ', 'ử': u'ử', 'ữ': u'ữ', 'ự': u'ự',
                         'ì': u'ì', 'í': u'í', 'ỉ': u'ỉ', 'ĩ': u'ĩ', 'ị': u'ị', 'ỳ': u'ỳ', 'ý': u'ý', 'ỷ': u'ỷ',
                         'ỹ': u'ỹ', 'ỵ': u'ỵ'}

    _MAP_DICT_UNICODE_UPPER = {'À': u'À', 'Á': u'Á', 'Ả': u'Ả', 'Ã': u'Ã', 'Ạ': u'Ạ', 'Ằ': u'Ằ', 'Ắ': u'Ắ',
                               'Ẳ': u'Ẳ', 'Ẵ': u'Ẵ', 'Ặ': u'Ặ',
                               'Ầ': u'Ầ', 'Ấ': u'Ấ', 'Ẩ': u'Ẩ', 'Ẫ': u'Ẫ', 'Ậ': u'Ậ', 'È': u'È', 'É': u'É',
                               'Ẻ': u'Ẻ', 'Ẽ': u'Ẽ', 'Ẹ': u'Ẹ', 'Ề': u'Ề', 'Ế': u'Ế',
                               'Ể': u'Ể', 'Ễ': u'Ễ', 'Ệ': u'Ệ', 'Ò': u'Ò', 'Ó': u'Ó', 'Ỏ': u'Ỏ', 'Õ': u'Õ',
                               'Ọ': u'Ọ', 'Ồ': u'Ồ', 'Ố': u'Ố', 'Ổ': u'Ổ', 'Ỗ': u'Ỗ',
                               'Ộ': u'Ộ', 'Ờ': u'Ờ', 'Ớ': u'Ớ', 'Ở': u'Ở', 'Ỡ': u'Ỡ', 'Ợ': u'Ợ', 'Ù': u'Ù',
                               'Ú': u'Ú', 'Ủ': u'Ủ', 'Ũ': u'Ũ', 'Ụ': u'Ụ', 'Ừ': u'Ừ',
                               'Ứ': u'Ứ', 'Ử': u'Ử', 'Ữ': u'Ữ', 'Ự': u'Ự', 'Ì': u'Ì', 'Í': u'Í', 'Ỉ': u'Ỉ',
                               'Ĩ': u'Ĩ', 'Ị': u'Ị', 'Ỳ': u'Ỳ', 'Ý': u'Ý', 'Ỷ': u'Ỷ',
                               'Ỹ': u'Ỹ', 'Ỵ': u'Ỵ'}

    _PREFIX = ["tỉnh", "quận", "huyện", "thị xã", "thành phố", "tp", "phường số", "phường", "xã", 'thị trấn',
               'đường số', 'đường', 'phố']

    _PREFIX_ACRONYM_MAPPER = {'bệnh viện': 'bv',
                              'tòa nhà': 'tn', 'chung cư': 'cc',
                              'khu đô thị': 'kđt', 'khu dân cư': 'kdc', 'khu biệt thự': 'kbt',
                              'khu tái định cư': 'khu tdc',
                              'khu công nghiệp': 'kcn', 'khu cn': 'kcn', 'khu công nghệ cao': 'khu cnc',
                              'khu chế xuất': 'kcx',
                              'tiểu học': 'th', 'cấp 1': 'c1',
                              'trung học cơ sở': 'thcs', 'cấp 2': 'c2',
                              'trung học phổ thông': 'thpt', 'cấp 3': 'thpt',
                              'mầm non': 'mn',  # chưa gặp viết tắt 'mẫu giáo' bao giờ
                              'khu phố': 'kp', 'kpho': 'kp', 'khph': 'kp', 'kph': 'kp',
                              'tổ dân phố': 'tdp',
                              'số nhà': 'sn'
                              }

    _PREFIX_ACRONYM_MAPPER_NO_ACCENT = {'benh vien': 'bv',
                                        'toa nha': 'tn', 'chung cu': 'cc',
                                        'khu do thi': 'kđt', 'khu dan cu': 'kdc', 'khu biet thu': 'kbt',
                                        'khu tai dinh cu': 'khu tdc', 'khu tập thể': 'ktt',
                                        'khu cong nghiep': 'kcn', 'khu cn': 'kcn', 'khu cong nghe cao': 'khu cnc',
                                        'khu che xuat': 'kcx',
                                        'tieu hoc': 'th', 'cap 1': 'c1', 'trung hoc co so': 'thcs', 'cap 2': 'c2',
                                        'trung hoc pho thong': 'thpt', 'cap 3': 'thpt',
                                        'mam non': 'mn',
                                        'khu pho': 'kp', 'kpho': 'kp', 'khph': 'kp', 'kph': 'kp',
                                        'to dan pho': 'tdp'
                                        }

    _SEARCH_MAPPER = {"AddressId": "id", "Name": "name", "Ward": "ward", "District": "district", "Province": "province",
                      "FullName": "level4_address",
                      "StreetId": "street_id",
                      "WardId": "ward_id", "DistrictId": "district_id", "ProvinceId": "province_id",
                      }

    @classmethod
    def search_mapper(cls):
        return cls._SEARCH_MAPPER

    @classmethod
    def json_to_dict(cls, s: str):
        return json.loads(s)

    @classmethod
    def to_our_key(cls, doc: dict):

        for rnd_key, our_key in cls._SEARCH_MAPPER.items():
            doc[our_key] = doc.pop(rnd_key, None)

        del doc['Type'], doc['Level']

        return doc

    @classmethod
    def to_our_key_batch(cls, doc_list: list):
        return list(map(cls.to_our_key, doc_list))

    @classmethod
    def normalize_text(cls, s: str, remove_accent=True) -> str:
        """
        lower case, convert to unicode, remove non-alphanumeric characters, redundant spaces
        """

        s = str(s).lower()
        s = cls.unicode_converter(s, convert_upper=False)
        if remove_accent:
            s = cls.remove_accent(s, keep_upper=False)
        s = cls.drop_special_char(s)
        s = cls.remove_tripling_char(s)
        s = cls.character_number_separator(s)
        s = re.sub(r"\s+", " ", s).strip()  # remove redundant spaces
        return s

    @classmethod
    def normalize_doc(cls, doc: dict, fields: tuple) -> dict:
        """
        Normalize the input doc (dict like) at specified fields
        :param doc: input
        :param fields: tuple of key in which their value will be normalized
        :return: normalized doc
        """
        for f in fields:
            doc[f] = cls.normalize_text(doc[f], remove_accent=False)
        return doc

    @staticmethod
    def apply_func_on_fields(func, doc: dict, fields: tuple) -> dict:
        """
        Apply a function specified fields of a dictionary
        :param func: Applying function
        :param doc: dictionary which will be processed
        :param fields: fields of doc to be processed
        :return: processed doc
        """
        for f in fields:
            doc[f] = func(doc[f])
        return doc

    @classmethod
    def remove_symbols(cls, s: str, symbols='|\(') -> str:
        """
        Remove specified symbols from input string
        :param s: input string
        :param symbols: removing symbols input string format
        :return: processed string
        """
        s = re.sub(rf"([{symbols}])", " ", s)  # remove symbols
        s = re.sub(r"\s+", " ", s).strip()  # remove redundant spaces
        return s

    @classmethod
    def unicode_converter(cls, s: str, convert_upper=True) -> str:
        for key in cls._MAP_DICT_UNICODE:
            s = s.replace(key, cls._MAP_DICT_UNICODE[key])
        if convert_upper:
            for key in cls._MAP_DICT_UNICODE_UPPER:
                s = s.replace(key, cls._MAP_DICT_UNICODE_UPPER[key])
        return s

    @staticmethod
    def remove_tripling_char(s: str) -> str:
        """
        if there at least 3 same chars consecutive, replace with only 2 chars
        e.g hhaaai -> hhaai
        """
        s = re.sub(r'(\w)\1\1*', r'\1\1', s)
        return s

    @staticmethod
    def drop_special_char(s: str) -> str:
        s = re.sub(r"([,.:;\(\)_\[\]<>?|\"\'+=*&^%$#@!~`])", " ", s)  # remove non-alphanumeric chars
        s = re.sub(r"[\\]", " ", s)  # remove slice char
        return s

    @staticmethod
    def remove_accent(text: str, keep_upper=False) -> str:
        """remove all accent characters of a  string
        return non-accent string"""
        text = str(text)  # make sure input is a string
        if keep_upper:
            text = re.sub(r"[ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴÀ̀]", "A", text)
            text = re.sub(r"[Đ]", "D", text)
            text = re.sub(r"[ÝỲỴỶỸ]", "Y", text)
            text = re.sub(r"[ÚÙỤỦŨ]", "U", text)
            text = re.sub(r"[ƯỨỪỰỬỮ]", "U", text)
            text = re.sub(r"[ÉÈẸẺẼ]", "E", text)
            text = re.sub(r"[ÊẾỀỆỂỄ]", "E", text)
            text = re.sub(r"[ÓÒỌỎÕ]", "O", text)
            text = re.sub(r"[ÔỐỒỘỔỖƠỚỚƠỜỢỞỠỘ]", "O", text)
            text = re.sub(r"[ÍÌỊỈĨ]", "I", text)
        else:
            text = text.lower()

        text = re.sub(r"[áàạảãâấầậẩẫăắằặẳẵà]", "a", text)
        text = re.sub(r"[đ₫ð]", "d", text)
        text = re.sub(r"[ýỳỵỷỹ]", "y", text)
        text = re.sub(r"[úùụủũ]", "u", text)
        text = re.sub(r"[ưứừựửữ]", "u", text)
        text = re.sub(r"[éèẹẻẽ]", "e", text)
        text = re.sub(r"[êếềệểễ]", "e", text)
        text = re.sub(r"[óòọỏõ]", "o", text)
        text = re.sub(r"[ôốồộổỗớơờợởỡộ]", "o", text)
        text = re.sub(r"[íìịỉĩ]", "i", text)

        return text

    @classmethod
    def remove_prefix(cls, normalized_string: str, prefix=None) -> str:
        """
        Remove address prefixes, except Quận 1, Phường 2...
        prefix: Tỉnh, Quận, Huyện, Thị xã, Thành phố, Phường, Xã, Thị trấn, Đường số, Đường
        except case: Quận 1, Phường 2, Đường 3, Đường số 4...
        """
        if prefix is None:
            prefix = cls._PREFIX
        # append no accent prefix
        prefix = prefix + [cls.remove_accent(pre) for pre in prefix]

        s = str(normalized_string).strip()
        for pre in prefix:
            if re.match(rf"{pre} \d+", s) is not None:
                break
            else:
                match = re.match(f"{pre} ", s)
                if match is not None:
                    s = re.sub(f"^{pre} ", " ", s)
                    break
        return re.sub(r"\s+", " ", s.strip())

    @staticmethod
    def character_number_separator(s: str) -> str:
        """
        :param s: input string
        :return: space separated string on number, character and [-/]
        """
        return ' '.join(re.findall(
            "[a-zA-Záàạảãâấầậẩẫăắằặẳẵàđýỳỵỷỹúùụủũưứừựửữéèẹẻẽêếềệểễóòọỏõôốồộổỗơớờợởỡộíìịỉĩ]+|[0-9]+|[-/$]", s))

    @staticmethod
    def first_char_acronym(s: str):
        """
        Extract acronym of a string, except number.
        :param s: string.lower()
        :return: first-char-only string

        Use before character_number_separator() and number_alias()
        """
        acronym = ''
        for word in s.split():
            if word.isnumeric():
                acronym = acronym + word
            else:
                acronym = acronym + word[0]
        return acronym

    @classmethod
    def prefix_mapper_acronym(cls, spa_string: str) -> str:
        for prefix, acronym in cls._PREFIX_ACRONYM_MAPPER.items():
            spa_string = spa_string.replace(prefix, acronym)
        return spa_string

    @classmethod
    def prefix_mapper_acronym_no_accent(cls, spa_string: str) -> str:
        for prefix, acronym in cls._PREFIX_ACRONYM_MAPPER_NO_ACCENT.items():
            spa_string = spa_string.replace(prefix, acronym)
        return spa_string

    @staticmethod
    def number_alias(text: str, alias="<num>", separate_only=False) -> str:
        """substitute all number in the string with representation string alias"""
        alias = str(alias)
        if separate_only:
            text = re.sub(r"\s\d+\s", " " + alias + " ", text)
            text = re.sub(r"^\d+\s", alias + " ", text)  # at the beginning of string
            text = re.sub(r"\s\d+$", " " + alias, text)  # at the end of string
        else:
            text = re.sub(r"\d+", alias, text)
        return text

    @staticmethod
    def remove_number(text: str) -> str:
        """remove number within the input text"""
        text = str(text)
        text = re.sub(r"\d+", " ", text)
        return text


if __name__ == '__main__':
    print("-----------NormUnicode class testing-----------")
    print(NormUnicode.normalize_text("""ấp 4_thường" 'tân^_ [bắc]. {tân uyên}_ <bình> dương"""))


class AddressAcronym(ABC):
    """
    Base class for address string processing
    """

    @classmethod
    @abstractmethod
    def extract_acronym(cls, string):
        """
        :param string: input string
        :return: acronym string
        """
        pass

    @classmethod
    def combine(cls, string, number_alias, normalize=True, include_origin_string=True):
        """
        normalize (option) -> extract_acronym -> number alias -> combine with original string
        :param string: input string
        :param number_alias: alias symbol for separate number in input string. If None, no alias is applied
        :param normalize: True, input string will be normalize, others: No normalize is performed
        :param include_origin_string: include the input string in result, acronym in parentheses
        :return:
        """
        if normalize:
            string = NormUnicode.normalize_text(string, remove_accent=False)

        acronym = cls.extract_acronym(string)

        # number alias
        if number_alias is not None:
            acronym = NormUnicode.number_alias(acronym, alias=number_alias, separate_only=False)
            if include_origin_string:
                string = NormUnicode.number_alias(string, alias=number_alias, separate_only=True)

        # combine
        if include_origin_string:
            result = string + " (" + acronym + ")"
        else:
            result = acronym

        return result

    @classmethod
    @abstractmethod
    def process(cls, string, include_origin_string=False):
        pass


class NormalAddressAcronym(AddressAcronym):

    @classmethod
    def extract_acronym(cls, string):
        # extract acronym string
        acronym = NormUnicode.first_char_acronym(string)

        return acronym

    @classmethod
    def process(cls, string: str, include_origin_string=True, normalize=False):
        return cls.combine(string, number_alias=None, normalize=normalize,
                           include_origin_string=include_origin_string)


class SpecialAddressAcronym(AddressAcronym):

    @classmethod
    def extract_acronym(cls, string):
        # extract acronym string
        acronym = NormUnicode.prefix_mapper_acronym(string)
        acronym = NormUnicode.prefix_mapper_acronym_no_accent(acronym)
        acronym = NormUnicode.character_number_separator(acronym)

        return acronym

    @classmethod
    def process(cls, string, include_origin_string=True, normalize=False):
        return cls.combine(string, number_alias=None, normalize=normalize,
                           include_origin_string=include_origin_string)

    @classmethod
    def process_batch(cls, string_list: list, include_origin_string=True,
                      normalize=False) -> list:
        """
        Apply cls.process() for list of string
        :param string_list: list of address string
        :param include_origin_string: include the input string in result, acronym in parentheses
        :param normalize: apply normalizing operation on input
        :return list of processed string
        """
        return list(map(lambda string: cls.process(
            string, include_origin_string=include_origin_string, normalize=normalize),
                        string_list))


class AddressKeywordAcronym:
    """
    Processor for special address and its keyword
    """

    @staticmethod
    def keyword_extractor(ads_data_list) -> list:
        """
        Extract the keyword string from ads query results (list)
        :param ads_data_list: list of address service data
        :return: list of keyword string
        """
        keyword_list = []
        for item in ads_data_list:
            if item[2] in (None, ''):
                keyword_list.append(item[3])  # prefix + " " + name
                continue
            elif item[3] in (None, ''):
                keyword_list.append(item[2])  # prefix + " " + name
                continue
            else:
                keyword_list.append(str(item[2]) + " " + str(item[3]))  # prefix + " " + name

        return keyword_list

    @classmethod
    def keyword_normalizer(cls, keyword_list):
        return list(set(map(lambda s: NormUnicode.normalize_text(s, remove_accent=False), keyword_list)))

    @classmethod
    def process(cls, ads_data_list: list, include_origin_string=True,
                normalize=False) -> list:
        if len(ads_data_list) != 0:
            keyword_list = cls.keyword_extractor(ads_data_list)
            keyword_list = cls.keyword_normalizer(keyword_list)

            return SpecialAddressAcronym.process_batch(
                keyword_list,
                include_origin_string=include_origin_string,
                normalize=normalize
            )
        else:
            return []


class NormalAddressAcronymWrapper:
    """
    Apply NormalAddressAcronym processing to related fields of input documents
    """

    def __init__(self, processor_obj: NormalAddressAcronym,
                 applying_fields=('ward', 'district', 'province'),
                 normalize=False):
        self.processor = processor_obj
        self.fields = applying_fields

    def process(self, doc, normalize=False):
        for field in self.fields:
            doc["p_" + field] = self.processor.process(
                doc[field], normalize=normalize)

        return doc


class AddressNameProcessing(ABC):

    @abstractmethod
    def process(self, doc, normalize=False):
        pass


class SpecialAddressNameProcessing(AddressNameProcessing):
    """
    Ultimate processing class for special address name
    """

    def __init__(self, spa_field='name', ads_field='address_service_data', keyword_separator=" | ",
                 include_origin_string=True):
        self.spa_field = spa_field
        self.ads_field = ads_field
        self.keyword_separator = keyword_separator
        self.include_origin_string = include_origin_string

    def process(self, doc, normalize=False):
        # process the main special address name (on ghtk db)
        main_name = SpecialAddressAcronym.process(doc[self.spa_field],
                                                  include_origin_string=self.include_origin_string,
                                                  normalize=normalize)
        # process the keyword (queried from address_service table)
        keywords = AddressKeywordAcronym.process(doc[self.ads_field],
                                                 include_origin_string=self.include_origin_string,
                                                 normalize=normalize)

        # combine into full processed string for special address name
        doc['p_name'] = main_name  # assign main_name
        # append processed keyword
        for kw in keywords:
            doc['p_name'] = doc['p_name'] + self.keyword_separator + kw

        return doc


class NormalAddressNameProcessing(AddressNameProcessing):
    """
    Ultimate processing class for normal address name
    """

    def __init__(self, normal_field='name'):
        self.normal_field = normal_field

    def process(self, doc, normalize=False):
        return NormalAddressAcronym.process(doc[self.normal_field], normalize=normalize)


class FullAddressStringProcessing(ABC):

    @abstractmethod
    def process(self, doc, normalize=False):
        pass


class FullSpecialAddressStringProcessing(FullAddressStringProcessing):
    """
    Process special address full string
    """

    def __init__(self,
                 special_address_acronym_obj: SpecialAddressAcronym,
                 normal_address_acronym_obj: NormalAddressAcronym,
                 special_keyword_obj: AddressKeywordAcronym,
                 full_string_separator=" | ", keyword_name_separator=" | "):
        self.spa_processor = special_address_acronym_obj
        self.norm_processor = normal_address_acronym_obj
        self.kw_processor = special_keyword_obj
        self.full_sep = full_string_separator
        self.kw_sep = keyword_name_separator

    def process(self, doc, normalize=False):
        # Normalize the full l4_address
        # doc['l4_address'] = NormUnicode.normalize_text(doc['level4_address'], remove_accent=False)

        # prepare name and keywords
        name = self.spa_processor.process(doc['name'],
                                          include_origin_string=False,
                                          normalize=normalize)

        processed_keywords = self.kw_processor.process(doc['address_service_data'],
                                                       include_origin_string=True,
                                                       normalize=normalize)
        kws_string = self.kw_sep.join(processed_keywords)  # combine processed kws into a string

        # other fields
        ward = self.norm_processor.extract_acronym(doc['ward'])
        district = self.norm_processor.extract_acronym(doc['district'])
        province = self.norm_processor.extract_acronym((doc['province']))

        # full_origin_string = NormUnicode.number_alias(doc['level4_address'], alias='$', separate_only=True)
        full_origin_string = doc['level4_address']

        # combine full string
        doc['p_l4_address'] = full_origin_string + \
                              self.full_sep + name + self.kw_sep + kws_string + self.kw_sep + \
                              ward + " " + district + " " + province
        return doc


class FullNormalAddressStringProcessing(FullAddressStringProcessing):
    """
    Process normal address full string
    """

    def __int__(self, normal_address_processing_obj: NormalAddressNameProcessing):
        self.processor = normal_address_processing_obj

    def process(self, doc, normalize=False):
        pass


class L123AddressCombine:
    """
    Return string "lev1_lev2_lev3 lev1_lev2_viet-tắt_lev3, lev1_viet-tắt-lev2_lev3, ..." of an address
    """

    @classmethod
    def extract_acronym(cls, string):
        # extract acronym string
        acronym = NormUnicode.first_char_acronym(string)

        return acronym

    @classmethod
    def process(cls, doc):
        l1_set = {doc['province'], cls.extract_acronym(doc['province'])}
        l2_set = {doc['district'], cls.extract_acronym(doc['district'])}
        l3_set = {doc['ward'], cls.extract_acronym(doc['ward'])}

        result_list = list()
        for l1 in l1_set:
            for l2 in l2_set:
                for l3 in l3_set:
                    result_list.append("_".join(l3.split() + l2.split() + l1.split()))
        doc['l123_combine'] = " ".join(result_list)

        return doc


class AddressDocumentProcessing:
    fp = FullSpecialAddressStringProcessing(SpecialAddressAcronym(),
                                            NormalAddressAcronym(),
                                            AddressKeywordAcronym()
                                            )
    sp = SpecialAddressNameProcessing()
    np = NormalAddressAcronymWrapper(NormalAddressAcronym())
    l123_p = L123AddressCombine()
    include_string_origin = True
    normalize = True

    def set_include_string_origin(self, value):
        self.include_string_origin = value

    @classmethod
    def process(cls, doc):
        cls.np.process(doc, normalize=cls.normalize)
        cls.sp.process(doc, normalize=cls.normalize)
        cls.fp.process(doc, normalize=cls.normalize)
        cls.l123_p.process(doc)

        return doc


if __name__ == "__main__":
    NormalAddressAcronym.process('số 1 / 2 Hà Bá tòa nhà bitexco financial')
